//react imports
import React from 'react';

//libraries and external imports
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

//components
import TabIcon from './src/components/TabIcon';

//screens
import HomeScreen from './src/screens/HomeScreen';
import TournamentDetailScreen from './src/screens/TournamentDetailScreen';
import ProfileScreen from './src/screens/ProfileScreen';

//navigators
const HomeStack = createStackNavigator();
const MainTab = createBottomTabNavigator();

function HomeStackNavigator() {
	return (
		<HomeStack.Navigator initialRouteName="Home">
			<HomeStack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }}/>
			<HomeStack.Screen name="Details" component={TournamentDetailScreen} options={{ headerShown: false }}/>
		</HomeStack.Navigator>
	)
}

function App(){
  return (
    <NavigationContainer>
      <MainTab.Navigator 
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'home-focused' : 'home';
            }
            else if (route.name === 'Profile') {
              iconName = focused ? 'profile-focused' : 'profile';
            }
            return <TabIcon name={iconName} />;
          }
        })}
        tabBarOptions={{
          showLabel: false,
          style: {
            backgroundColor: 'black',
            borderTopColor: '#292929'
          }
        }}
      >
        <MainTab.Screen name="Home" component={HomeStackNavigator} />
        <MainTab.Screen name="Profile" component={ProfileScreen} />
      </MainTab.Navigator>
    </NavigationContainer>
  )
};

export default App;
