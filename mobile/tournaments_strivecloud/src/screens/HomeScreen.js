import React, { useEffect, useState } from 'react';
import { Text, ScrollView, View, StyleSheet } from 'react-native';

//libraries
import axios from 'axios';

//components
import TournamentItem from '../components/TournamentItem';
import LoadingScreen from '../components/Loading';
import TournamentDetailScreen from './TournamentDetailScreen';

function HomeScreen({ navigation }) {
  const [tournaments, setTournaments] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get(`https://api.kayzr.com/api/tournaments/upcoming`)
      .then(res => {
        const nameList = res.data;
        setTournaments(nameList);
        setLoading(false);
      })
  }, [])

  function redirectToDetailsScreen(tournament) {
    navigation.navigate('Details', {
      tournament: tournament
    });
  }

  return (
    <View style={styles.homeContainer}>
      {loading === true ? (
        <LoadingScreen />
      ) : (
          <ScrollView>
            <View style={styles.homeHeader}>
              <Text style={styles.homeHeaderTitle}>Tournaments</Text>
            </View>
            <View style={styles.homeTournamentList}>
              {tournaments.map((tournament, key) => 
              (
                <TournamentItem
                  day={new Date(tournament.date).getDay()}
                  month={"September"}
                  time={"17:00"}
                  title={tournament.name}
                  tournamentClickHandler={() => redirectToDetailsScreen(tournament)}
                  tournament={tournaments}
                  key={key}
                />
              ))}
              {/*<TournamentItem
          day={"30"}
          month={"August"}
          time={"17:00"}
          title={"ALTERNATE - Call of Duty: Modern Warfare 5v5 | S&D - Hardpoint - Domination | Crossplay"}
          tournamentClickHandler={() => redirectToDetailsScreen()}
        />
        <TournamentItem
          day={"30"}
          month={"August"}
          time={"17:00"}
          title={"Fortnite Killrace | 1v1 Crossplay | Presented by IceWolves x VGComputers"}
        />
        <TournamentItem
          day={"30"}
          month={"August"}
          time={"17:00"}
          title={"IceWolves and VG computers present: Valorant | 5v5"}
        />
        <TournamentItem
          day={"30"}
          month={"August"}
          time={"17:00"}
          title={"Brawlhalla | 2v2 | Crossplay"}
        />
        <TournamentItem
          day={"30"}
          month={"August"}
          time={"17:00"}
          title={"Rocket League | 3v3 | Crossplay"}
        />*/}
            </View>
          </ScrollView>)}
    </View >
  );
}

const styles = StyleSheet.create({
  homeContainer: {
    flex: 1,
    backgroundColor: '#000'
  },
  homeHeader: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  homeHeaderTitle: {
    color: '#FFF',
    fontSize: 18,
    fontWeight: 'bold'
  },
  homeTournamentList: {
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
  }
});


export default HomeScreen;