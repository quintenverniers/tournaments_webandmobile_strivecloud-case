
import * as React from 'react';
import { Text, ScrollView, View, StyleSheet, TouchableOpacity, ImageBackground } from 'react-native';


//assets
import gameImage from '../assets/fortnite.jpg';

//components

function TournamentDetailScreen({ route, navigation }) {
    const {tournament} = route.params;
    return (
        <ScrollView style={styles.detailContainer}>
            <View style={styles.detailTopSection}>
                <ImageBackground source={gameImage} style={styles.detailGameImage}>
                    <TouchableOpacity style={styles.detailHeader} onPress={() => navigation.goBack()}>
                        <Text style={styles.detailHeaderTitle}>Back</Text>
                    </TouchableOpacity>
                </ImageBackground>
            </View>
            <View style={styles.detailContent}>
                <View style={styles.detailContentItem}>
                    <Text style={styles.detailContentItemTitle}>Game</Text>
                    <Text style={styles.detailContentItemText}>{tournament.name}</Text>
                </View>
                <View style={styles.detailContentItem}>
                    <Text style={styles.detailContentItemTitle}>Time</Text>
                    <View style={styles.detailContentSubItem}>
                        <Text style={styles.detailContentItemSubTitle}>Start</Text>
                        <Text style={styles.detailContentItemText}>17:30</Text>
                    </View>
                    <View style={styles.detailContentSubItem}>
                        <Text style={styles.detailContentItemSubTitle}>End</Text>
                        <Text style={styles.detailContentItemText}>17:30</Text>
                    </View>
                </View>
                <View style={styles.detailContentItem}>
                    <Text style={styles.detailContentItemTitle}>Location</Text>
                    
                        <Text style={styles.detailContentItemText}>{tournament.location}</Text>
                </View>
                <View style={styles.detailContentItem}>
                    <Text style={styles.detailContentItemTitle}>Check-in</Text>
                    <View style={styles.detailContentSubItem}>
                        <Text style={styles.detailContentItemSubTitle}>Start</Text>
                        <Text style={styles.detailContentItemText}>17:30</Text>
                    </View>
                    <View style={styles.detailContentSubItem}>
                        <Text style={styles.detailContentItemSubTitle}>End</Text>
                        <Text style={styles.detailContentItemText}>17:30</Text>
                    </View>
                </View>
                <View style={styles.detailContentItem}>
                    <Text style={styles.detailContentItemTitle}>Rewards</Text>
                    
                        <Text style={styles.detailContentItemText}>Online</Text>
                </View>
                <TouchableOpacity style={styles.detailSubmitButton}>
                    <Text style={styles.detailSubmitButtonText}>View tournament</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    detailContainer: {
        flex: 1,
        backgroundColor: '#000'
    },
    detailTopSection: {
        height: 250,
        backgroundColor: 'blue'
    },
    detailGameImage: {
        width: '100%',
        height: '100%'
    },
    detailHeader: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
    },
    detailHeaderTitle: {
        color: '#FFF',
        fontSize: 18,
        fontWeight: 'bold'
    },
    detailContent: {
        marginTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        //backgroundColor: 'red'
    },
    detailContentItem: {
        marginBottom: 20,
    },
    detailContentSubItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 4,
    },
    detailContentItemTitle: {
        fontWeight: 'bold',
        color: '#949494',
        marginBottom: 4,
        fontSize: 16,
    },
    detailContentItemSubTitle: {
        fontWeight: '600',
        color: '#BEBEBE',
        fontSize: 16,
    },
    detailContentItemText: {
        color: '#d3d3d3',
        fontSize: 16
    },
    detailSubmitButton: {
        width: '100%',
        height: 40,
        backgroundColor: '#00F2D9',
        marginBottom: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    detailSubmitButtonText: {
        fontWeight: 'bold'
    }
});


export default TournamentDetailScreen;