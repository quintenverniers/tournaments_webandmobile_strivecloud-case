import React from 'react';
import { Text, ScrollView, View, StyleSheet } from 'react-native';

//libraries

function ProfileScreen() {
  return (
    <View style={styles.profileContainer}>
        <Text style={styles.profileText}>Profile</Text>
    </View >
  );
}

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
    backgroundColor: '#000'
  },
  profileText: {
      color: '#FFF'
  }
  
});


export default ProfileScreen;