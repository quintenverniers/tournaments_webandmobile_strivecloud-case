import React from 'react';
import {
	StyleSheet,
	View,
    Image,
} from 'react-native';

import loadingImg from '../assets/maxresdefault.jpg'

function LoadingScreen() {
      
	return (
		<View style={styles.loadingContainer}>
            <Image source={loadingImg} style={styles.image}/>
        </View>
	)
}

const styles = StyleSheet.create({
	loadingContainer: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        width: 160,
        height: 160,
    }
});

export default LoadingScreen;
