import React from 'react';
import {
	StyleSheet,
	View,
    Image,
    Text,
    TouchableOpacity,
    ImageBackground
} from 'react-native';

//assets
import gameImage from '../assets/fortnite.jpg';

function TournamentItem(props) {
      
	return (
		<TouchableOpacity style={styles.tournamentItemContainer} onPress={() => props.tournamentClickHandler()}>
            <View style={styles.TournamentItemContentColumn}>
                <View style={styles.TournamentItemDate}>
                    <Text style={styles.TournamentDateDay}>{props.day}</Text>
                    <View style>
                        <Text style={styles.TournamentDateMonth}>
                            {props.month}
                        </Text>
                        <Text style={styles.TournamentDateTime}>
                            {props.time}
                        </Text>
                    </View>
                </View>
                <View>
                    <Text style={styles.TournamentTitle}>
                        {props.title}
                    </Text>
                </View>
            </View>
            <View style={styles.TournamentItemImageColumn}>
                <ImageBackground source={gameImage} style={styles.TournamentGameCover}>

                </ImageBackground>
            </View>
		</TouchableOpacity>
	)
}

const styles = StyleSheet.create({
	tournamentItemContainer: {
        height: 180,
        width: '100%',
        backgroundColor: '#1B1C1E',
        marginBottom: 20,
        flexDirection: 'row'
    },
    TournamentItemContentColumn: {
        width: '60%',
        height: '100%',
        paddingLeft: 10,
        paddingRight: 10,
    },
    TournamentItemDate: {
        width: '100%',
        paddingTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    TournamentDateDay: {
        fontSize: 42,
        color: '#FFF',
        margin: 0,
        padding: 0,
        marginRight: 10,
    },
    TournamentDateMonth: {
        color: '#FFF', 
        fontSize: 18,   
    },
    TournamentDateTime: {
        color: '#FFF', 
    },
    TournamentTitle: {
        color: '#00F2D9'
    },
    TournamentItemImageColumn: {
        width: '40%',
        backgroundColor: 'red',
    },
	TournamentGameCover: {
		width: '100%',
		height: '100%'
	}
});

export default TournamentItem;
