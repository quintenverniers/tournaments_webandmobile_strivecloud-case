import React from 'react';
import {
	StyleSheet,
	View,
	Image,
} from 'react-native';
import homeIcon from '../assets/home.png';
import homeFocusedIcon from '../assets/home_focused.png';
import profileIcon from '../assets/profile.png';
import profileFocusedIcon from '../assets/profile_focused.png';

function TabIcon(props) {
	let iconSource;
	switch (props.name) {
		case "home":
			iconSource = homeIcon;
            break;
        case "home-focused":
			iconSource = homeFocusedIcon;
			break;
		case "profile":
			iconSource = profileIcon;
            break;
        case "profile-focused":
			iconSource = profileFocusedIcon;
			break;
		default: 
			iconSource = homeIcon;
			break;
	}
	return (
		<View style={styles.tabIconContainer}>
			<Image source={iconSource} style={styles.Image}/>
		</View>
	)
}

const styles = StyleSheet.create({
	tabIconContainer: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	Image: {
		width: 28,
		height: 28
	}
});

export default TabIcon;
