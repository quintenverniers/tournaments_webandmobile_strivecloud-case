import React from 'react';

function LoadingScreen() {
  return (
    <div className="loadingscreen__container">
        <h1>Loading...</h1>
    </div>
  );
}



export default LoadingScreen;