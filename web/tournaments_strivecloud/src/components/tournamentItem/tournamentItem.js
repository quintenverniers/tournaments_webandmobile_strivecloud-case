import React from 'react';

import './tournamentItem.css';
import gameImage from '../../assets/fortnite.jpg';

function TournamentItem(props) {
  return (
    <div className="tournamentItem__container" onClick={() => props.navigateToDetails()}>
        <div className="tournamentItem__content">
            <div className="tournamentItem__date__wrapper">
                <p className="tournament__date__day">30</p>
                <div className="tournament__date__details">
                    <p>
                        November
                    </p>
                    <p>
                        17:00
                    </p>
                </div>
            </div>
            <p className="tournamentTitle">{props.title}</p>
          </div>
            <div className="tournamentItem__image" style={{backgroundImage: "url(" + gameImage + ")"}}>

            </div>
    </div>
  );
}

export default TournamentItem;
