import React, { useEffect, useState } from 'react';
import './home.css';

//libraries
import axios from 'axios';

//components
import TournamentItem from '../../components/tournamentItem/tournamentItem';
import LoadingScreen from '../../components/loadingScreen/loadingScreen';

function Home() {
  const [tournaments, setTournaments] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    axios.get(`https://api.kayzr.com/api/tournaments/upcoming`)
      .then(res => {
        const tournamentList = res.data;
        setTournaments(tournamentList);
        setLoading(false);
      }).catch(err => {
        console.log(err);
      })
  }, []);

  function navigateToDetails() {
    alert('navigate');
  }

  return (
    <div className="home__container">
      {/*{loading === true ? 
        <LoadingScreen />
      : */}
          <div className="home__container">
            <div className="home__header">
              <h1 className="home__title">Tournaments</h1>
            </div>
            <div className="home__tournaments__list">
              <TournamentItem
                title={"Rainbow Six Siege | 5v5 | PS4 Only"}
                navigateToDetails={() => navigateToDetails()}
              />
              <TournamentItem
                title={"Rainbow Six Siege | 5v5 | PS4 Only"}
              />
            </div>
          </div>
    </div>
  );
}

export default Home;
